import maskpass


# creer un compte
def create_account():
    name = input("Entrez votre prénom:🐷 ")
    lastname = input("Entrez votre nom de famille:")
    streetnum = input("Entrez votre numéro de rue: ")
    streetname = input("Entrez votre nom de rue: ")
    postal = input("Entrez votre code postal: ")
    city = input("Entrez votre ville:")
    dept = input("Entrez votre département:")
    region = input("Entrez votre nom de région: ")
    country = input("Entrez votre pays: ")
    print("\n Créer un nouvel identifiant et un Password : \n ")
    id = input("Entrez votre identifier please: ")
    passwd = check_password()
    
    my_infos = [name, lastname, streetnum, streetname, postal, city, dept, region, country, id, passwd]
    return my_infos
    
# check password correct    
def check_password():
    # cache le mot de passe
    passwd = maskpass.askpass(prompt="Entrez your password please: ", mask="🦧")
    if maskpass.askpass(prompt="Entrez your password again please: ",mask="🐣") == passwd : 
        return passwd
    else:
        print("Mauvais mot de passe🐷")
        check_password()


# demander identifiants
def connexion(data):
    print("\nVeuillez entrer vos identifiants de connexion : \n")
    id = input("Entrer l'identifiant : ")
    passwd = maskpass.askpass(prompt="Entrer le passwd : ", mask="*")
    
    if id == data[9] and passwd == data[10]:
        print("Bonjour " + id)
    else:
        print("ERROR le mot de passe ou l'user ne correspond pas🐷")
        return connexion(data)

# print le wallet
def print_money(money):
    print(f"Vous avez actuellement : {money}MLMM\n")

# credit ou debit du compte
def operation(money):
    print_money(money)
    answer2 = input("Voulez vous créditer(c) ou débiter(d) votre compte MartyLucaMaximMoney (1MLMM = 5€) ? [c/d] ")
    if answer2 == "c":
        add = input("Entrez le nb de MLMM à créditer : ")
        prix = int(add) * 5
        print("Ca vous coutera " + str(prix) + "€")
        valider = input("[y] pour Valider, [n] pour Annuler : ")
        if valider == "y":
            money += int(add)
            print_money(money)
        else:
            print("transaction annulée. 🕺")
    elif answer2 == "d":
        debit = input("Combien de MLMM voulez-vous débiter ? :")
        if (int(debit) <= money):
            money -= int(debit)
            print_money(money)
        else:
            print(f"Vous êtes trop pauvre pour ça ! 🕺 Vous n'avez que {money} MLMM")
    else:
        print("ERREUR: Veuillez saisir seulement (c) pour créditer ou (d) pour débiter")
        return operation(money)



# simulation fichier json
data = ["marty", "blabla", "15", "rue random", "33000", "Bordeaux", "Gironde", "aquitaine", "France", "marty", "root"]

my_money = 0
    
while True:    
    answer = input("Voulez vous créer un compte ? [y/n]")
    if answer == "y":
        create_account()
        break   
    elif answer == "n":
        connexion(data)
        break
    else:
        print("ERREUR: Veuillez répondre par oui(y) ou par non(n) ")
        
operation(my_money)

